all:

deploy-workstation:
	-[ -x /usr/sbin/nft ] && sudo nft flush ruleset
	sudo ./iptables-workstation.sh
	sudo sh -c "iptables-save > rules.v4"
	sudo sh -c "ip6tables-save > rules.v6"
	-if [ -x /usr/sbin/netfilter-persistent ]; \
	then \
	  [ -d /etc/iptables ] && sudo mkdir /etc/iptables; \
	  sudo mv rules.v[46] /etc/iptables; \
	  sudo etckeeper vcs add iptables; \
	  if ! sudo etckeeper vcs diff --staged --no-patch --exit-code -- iptables; \
	  then \
	    sudo etckeeper vcs commit -m "iptables: update rules" -- iptables; \
	  fi; \
	  sudo systemctl restart netfilter-persistent.service; \
	fi

