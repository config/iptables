#!/bin/sh
#
# Some utility functions to keep iptables and ip6tables configuration in sync.
#
# Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>
# SPDX-License-Identifier: MIT
#
# Based on updateipt.sh by Phil Sutter:
# https://developers.redhat.com/blog/2017/01/10/migrating-my-iptables-setup-to-nftables/

# useful wrappers for failure analysis
cmd_or_print() { # command
  "$@" || echo "failed at: '$*'"
}

ipt() { # iptables params
  cmd_or_print iptables "$@"
}

ip6t() { # ip6tables params
  cmd_or_print ip6tables "$@"
}

# have a simple way of doing things in iptables and ip6tables in parallel
ip46t() { # ip(6)tables params
  ipt "$@"
  ip6t "$@"
}

# clear out everything
flush_ruleset() {
  for it in iptables ip6tables; do
    for table in filter mangle nat raw; do
      $it -t $table -nL >/dev/null 2>&1 || continue # non-existing table

      $it -t $table -F        # delete rules
      $it -t $table -X        # delete custom chains
      $it -t $table -Z        # zero counters
    done
  done
}
